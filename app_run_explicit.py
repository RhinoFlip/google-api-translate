from settings import Credentials

import re

from google.cloud.translate_v3 import TranslationServiceClient
from google.oauth2 import service_account

__all__ = ['Translation']

FULL_REGEX = r'^projects/[a-z]+-\d{6}/locations/(?:global|us-central1)'
PROJECT_REGEX = r'^[a-z]+-\d{6}$'


class Translation:

    name = 'Google Translate API v3',
    max_score = 90

    def __init__(self):
        credentials = service_account.Credentials.from_service_account_file(Credentials.GOOGLE_API_CREDENTIALS)
        self.client = TranslationServiceClient(credentials=credentials)

        if re.match(PROJECT_REGEX, Credentials.GOOGLE_PROJECT) is not None:
            self.parent = self.client.location_path(Credentials.GOOGLE_PROJECT, 'global')
        elif re.match(FULL_REGEX, Credentials.GOOGLE_PROJECT) is not None:
            self.parent = Credentials.GOOGLE_PROJECT
        else:
            raise Exception('Project path does not match project neither full path')

    def get_languages(self):
        return [
            l.language_code
            for l in self.client.get_supported_languages(self.parent).languages
        ]

    def translate(self, text, target, source=None):
        trans = self.client.translate_text(
            [text], target, self.parent, source_language_code=source
        )

        yield {
            "text"   : trans.translations[0].translated_text,
            "quality": self.max_score,
            "service": self.name,
            "source" : text,
        }


def main():
    t = Translation()
    translation = t.translate('Hello World', 'cs')
    print(translation)


if __name__ == '__main__':
    main()