from google.cloud.translate_v3 import TranslationServiceClient

client = TranslationServiceClient()

parent = client.location_path('translating-271108', 'global')

stringCs = 'Čau světe'

stringEn = 'Hello world'

sl = client.get_supported_languages(parent)

# dl = client.detect_language(parent, content='Čau světe!')

# lang = client.detect_language(parent, stringCs)

contents=[stringCs]

target_language="en"

response = client.translate_text(contents, target_language, parent)

print(response)

# print(sl)
