from unittest import TestCase
from unittest.mock import MagicMock, patch, Mock, PropertyMock
import settings


class LanguageObject(Mock):
    @property
    def language_code(self):
        return Mock(side_effect=['en', 'cs', 'es'])


class FakeLanguageSetObject(Mock):
    languages = [LanguageObject()]


class FakeTranslatorClient(Mock):
    def get_supported_languages(self, *args):
        return FakeLanguageSetObject()

    def translate_text(self, *args):
        return Mock()


class FakeCredentials(Mock):
    GOOGLE_PROJECT = 'project-123456'
    GOOGLE_API_CREDENTIALS = 'TOTALYFAKECREDENTIALS'


@patch('settings.Credentials', new=FakeCredentials)
class TestGoogleTranslate(TestCase):

    def test_credentials(self):
        self.assertEqual(settings.Credentials.GOOGLE_PROJECT, 'project-123456')
        self.assertEqual(settings.Credentials.GOOGLE_API_CREDENTIALS, 'TOTALYFAKECREDENTIALS')

    @patch('google.oauth2.service_account.Credentials')
    def test_credentials_how_Translator_sees_them(self, *args):
        from app_run_explicit import Translation
        t = Translation()
        t.client = FakeTranslatorClient()
        lang = t.get_languages()
        self.assertIn("en", list)

    '''
    @patch('google.oauth2.service_account.Credentials', new=Mock())
    def test_fetch_languages(self):
        t = Translation()
        t.client = FakeTranslatorClient()
        langs = t.get_languages()

    @patch('google.cloud.translate_v3.TranslationServiceClient', new=FakeTranslator)
    def test_faking_google_client(self):
        from google.cloud.translate_v3 import TranslationServiceClient
        c = TranslationServiceClient()
        trans = c.translate_text('whatever', 'cs')
        self.assertEqual(trans.translated_text, 'cokoliv')

    @patch('google.oauth2.service_account.Credentials')
    @patch('google.cloud.translate_v3.TranslationServiceClient', new=FakeTranslator)
    def test_translation(self, *args):
        t = Translation()
        trans = t.translate('Hello', 'cs')
        self.assertEqual(trans.translations[0].translated_text, 'Ahoj')
    '''